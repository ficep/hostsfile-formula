=========
hostsfile
=========

Formula to manage hosts file (``/etc/hosts``).

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``hostsfile.init``
------------------

Set the host aliases under ``/etc/hosts`` file according to pillar's values.

.. code:: yaml

    hostsfile:
        aliases:
            application-server-0: '10.11.12.13'
            application-server-1: '10.11.12.14'
            load-balancer-0: '20.0.0.1'

References
==========

-  `Salt Formulas <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`__
