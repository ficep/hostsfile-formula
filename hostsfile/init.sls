{% for hostname, ip in salt['pillar.get']('hostsfile:aliases', {}).iteritems() %}
    {% if salt['grains.get']('host') != hostname %}
remove_old_{{ hostname }}_definition:
    file.comment:
        - name: /etc/hosts
        - regex: ^.*{{ hostname }}.*$
        - char: '# commented by saltstack # '

add_{{ ip }}_{{ hostname }}:
    host.only:
        - name: {{ ip }}
        - hostnames:
            - {{ hostname }}.novalocal
            - {{ hostname }}
    {% endif %}
{% endfor %}
